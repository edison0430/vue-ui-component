import MyVueButton from "./components/MyVueButton.vue";
import MyVueInput from "./components/MyVueInput.vue";
import WrapperedQuasarButton from "./components/WrapperedQuasarButton.vue";

export default {
  install: (app, options) => {
    app.component("MyVueButton", MyVueButton);
    app.component("MyVueInput", MyVueInput);
    app.component("WrapperedQuasarButton", WrapperedQuasarButton);
  },
};

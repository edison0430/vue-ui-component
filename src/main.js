import { createApp } from "vue";
import { Quasar } from "quasar";
import "@quasar/extras/material-icons/material-icons.css";
import "quasar/src/css/index.sass";

import App from "./App.vue";
import CustomUIComponentPlugin from "./build-entry";

createApp(App).use(Quasar).use(CustomUIComponentPlugin).mount("#app");
